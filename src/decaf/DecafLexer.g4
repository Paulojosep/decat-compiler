
lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

tokens
{
  TK_class
}

IF : 'if';

ELSE : 'else';

TRUE : 'true';

FALSE : 'false';

BOOLEAN : 'boolean';

CALLOUT : 'callout';

CLASS : 'class';

INT : 'int';

RETURN : 'return';

VOID : 'void';

FOR : 'for';

BREAK : 'break';

CONTINUE : 'continue';

SUM : '+';

SUB : '-';

MULTI : '*';

DIV : '/';

MAIOR : '>';

MENOR : '<';

MAIORIGUAL : '>=';

MENORIGUAL : '<=';

EQUAL : '==';

DIFER : '!=';

RECEBE : '=';

SINAL_MAIS_IGUAL : '+=';

SINAL_MENOS_IGUAL : '-=';

EXCLAMACAO : '!';

AND : '&&';

OR : '||';

PARENTRIGHT : '(';

PARENTLEFT : ')';

KEYRIGTH : '{';

KEYLEFT : '}';

COLCHETERIGTH : '[';

COLCHETELEFT : ']';

POINT : '.';

TWOPOINT : ':';

VIRGULA : ',';

PONTOVIRGULA : ';';

WALL : '|';

WALLS : '||';

PORCENTAGEM : '%';

//ID : (LET|'_') (LET|'_'|NUM)*;

ID : ('a'..'z'|'A'..'Z'|'_')+ (NUMBER)* ID?;

WS_ : (' ' | '\n'|'\t' ) -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHAR : '\'' (ESC|NUM|SIMBOL|HEX|LET)'\'';

STRING : '\"' (WS_|ESC|ID|NUM|LET|SIMBOL|HEX|PORCENTAGEM)*'\"';

HEXLIT : '0x'(NUM|HEX|SIMBOL)+;

INTLIT : (HEXLIT|NUM)+;

NUM : (NUMBER)+;

LET : LETRA(LETRA)*;

fragment
ESC :  '\\' ('n'|'t'|'"'|'\\');

fragment
HEX : ('a'..'f'|'A'..'F'); 

fragment
NUMBER : ('0'..'9');

fragment
LETRA : ('a'..'z'|'A'..'Z');

fragment
SIMBOL : ('\\\''|'\\\"'|'.'|','|':'|';'|'?');
