package decaf;

import org.antlr.symtab.FunctionSymbol;
import org.antlr.symtab.GlobalScope;
import org.antlr.symtab.LocalScope;
import org.antlr.symtab.Scope;
import org.antlr.symtab.VariableSymbol;
import org.antlr.symtab.Symbol;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import java.util.ArrayList;

/**
 * This class defines basic symbols and scopes for Decaf language
 */
public class DecafSymbolsAndScopes extends DecafParserBaseListener {
    ParseTreeProperty<Scope> scopes = new ParseTreeProperty<Scope>();
    GlobalScope globals;
    Scope currentScope; // define symbols in this scope
    Boolean possuiMain = false;
    ArrayList<String> variaveis = new ArrayList();
    ArrayList<String> escopos = new ArrayList();
    ArrayList<String> metodos = new ArrayList();
    ArrayList<String> typeParams = new ArrayList();
    ArrayList<String> variaveisAndTypes = new ArrayList();
    Boolean typeVoid;
    String typeMethod;

    @Override
    public void enterProgram(DecafParser.ProgramContext ctx) {
        globals = new GlobalScope(null);

        for(int i=0; i<ctx.method_decl().size();i++){
            if(ctx.method_decl().get(i).ID().getText().equals("main")){
                this.possuiMain = true;
            }
        }
        pushScope(globals);
    }

    @Override
    public void exitProgram(DecafParser.ProgramContext ctx) {
        // popScope();
        System.out.println(globals);
        if (this.possuiMain == false) {
            this.error(ctx.method_decl().get(0).ID().getSymbol(), "No main method");
            System.exit(0);
        }
    }

    @Override
    public void enterMethod_decl(DecafParser.Method_declContext ctx) {
        String name = ctx.ID().getText();
        this.typeVoid = false;
        this.metodos.add("{" + name + "," + ctx.decl().size() + "}"); //{nome,quantidade de paramentro}

        for(int i=0;i<ctx.decl().size();i++){
            this.typeParams.add("{"+name+","+i+","+ctx.decl().get(i).type().getText()+"}"); // {nome, posição do parametro, tipo do parametro}
        }

        try{
           this.typeMethod = ctx.type().getText(); 
        }catch (Exception e){}

        try {
            if (ctx.VOID().getText().equals("void")) {
                this.typeVoid = true;
            }
        } catch (Exception e) {}

        // int typeTokenType = ctx.type().start.getType();
        // DecafSymbol.Type type = this.getType(typeTokenType);

        // push new scope by making new one that points to enclosing scope
        FunctionSymbol function = new FunctionSymbol(name);
        // function.setType(type); // Set symbol type

        currentScope.define(function); // Define function in current scope
        saveScope(ctx, function);
        pushScope(function);
    }

    @Override
    public void exitMethod_decl(DecafParser.Method_declContext ctx) {
        try {
            if (typeVoid == true) {
                for (int i=0;i<ctx.block().statem().size();i++) {
                    if (ctx.block().statem().get(i).RETURN().getText().equals("return")) {
                        this.error(ctx.block().statem().get(i).RETURN().getSymbol(), "should not return value");
                        System.exit(0);
                    }
                }
            }else{
                for(int i = 0;i<ctx.block().statem().size();i++){
                    //verifica o retorno
                    if(ctx.block().statem().get(i).RETURN().getText().equals("return")){
                        for(int j = 0;j<ctx.block().statem().get(i).expr().size();j++){
                            // Entra no literal (boolean ou int)
                            if(ctx.block().statem().get(i).expr().get(j).literal() != null){
                                String literal = ctx.block().statem().get(i).expr().get(j).literal().getText();

                                //caso o tipo seja 'boolean' ira receber valor diferente
                                if(this.typeMethod.equals("boolean")&& !(literal.equals("false") || literal.equals("true"))){
                                    System.out.println("Waiting for boolean");
                                    this.error(ctx.ID().getSymbol(),"return value has wrong type");
                                    System.exit(0);
                                }
                            }

                            // entra no location (variaveis)
                            if(ctx.block().statem().get(i).expr().get(i).location() != null){
                                
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {}
        popScope();
    }

    @Override
    public void enterMethod_call(DecafParser.Method_callContext ctx) {
        try{
            //Verificar se passou a quantidade de parametro correto
            if(!this.metodos.contains("{"+ctx.method_name().ID().getText()+","+ctx.expr().size()+"}")){
                this.error(ctx.method_name().ID().getSymbol(), "Argument mismatch");
                System.exit(0);
            }
            //verifica se passou o(s) tipo(s) do(s) parametros certo
            for(int i=0; i<ctx.expr().size();i++) {
                if(ctx.expr().get(i).literal() != null){
                    if(ctx.expr().get(i).literal().getText().equals("false") || ctx.expr().get(i).literal().getText().equals("true")){
                        if(!this.typeParams.contains("{"+ctx.method_name().ID().getText()+","+i+", boolean}")){
                            this.error(ctx.method_name().ID().getSymbol(), "Types don't match singature");
                            System.exit(0);
                        }
                    }else{
                        if(!this.typeParams.contains("{"+ctx.method_name().ID().getText()+","+i+",int}")){
			    this.error(ctx.method_name().ID().getSymbol(), "Types don't match singature");
                            System.exit(0);
                        }
                    }
                }
                // Entrando no location
                if(ctx.expr().get(i).location() != null){

                }
            }
        }catch(Exception e) {}
    }

    //@Override
    //public void exitMethod_call(DecafParser.Method_callContext ctx) { }

    @Override
    public void enterField_decl(DecafParser.Field_declContext ctx) {
	for(int i = 0; i<ctx.ID().size(); i++){
            try{
                // vreficar se criou um array vazio
                if(Integer.parseInt(ctx.int_literal().getText()) <= 0){
                    this.error(ctx.int_literal().INTLIT().getSymbol(), "bad array size");
                    System.exit(0);
                }
            }catch(Exception e) {}

            String field = ctx.ID().get(i).getSymbol().getText();
            this.variaveis.add(field);

            if(ctx.COLCHETERIGTH() ==null){
            //adicionando variaveis associados ao tipo
            this.variaveisAndTypes.add("{"+field+","+ctx.type().getText()+"}");
            }else{
                this.variaveisAndTypes.add("{"+field+","+ctx.type().getText()+","+"array"+"}");
            }
            VariableSymbol fieldSymbol = new VariableSymbol(field);
            currentScope.define(fieldSymbol);
        }
     }
	
    @Override
    public void exitField_decl(DecafParser.Field_declContext ctx) {
        for(int i=0; i<ctx.ID().size(); i++){
            String name = ctx.ID().get(i).getSymbol().getText();
            Symbol field = currentScope.resolve(name);

            if(field == null){
                this.error(ctx.ID().get(i).getSymbol(), "No such variable: " + name);
            }
            if(field instanceof FunctionSymbol){
                this.error(ctx.ID().get(i).getSymbol(), name + " is not variable");
            }
        }
    }

    @Override
    public void enterBlock(DecafParser.BlockContext ctx) {
    //  LocalScope l = new LocalScope(currentScope); saveScope(ctx, currentScope); pushScope(l);
    //  saveScope(ctx, currentScope);
    //  pushScope(l);      
    }
     
    @Override public void exitBlock(DecafParser.BlockContext ctx) {
    //    popScope();
    }

    @Override
    public void enterStatem(DecafParser.StatemContext ctx) {
        try {
            if(ctx.IF() != null){
                for(int i=0;i<ctx.expr().size();i++){
                    if(!ctx.expr().get(i).location().ID().getText().equals("true") || !ctx.expr().get(i).location().ID().getText().equals("false") || !this.variaveisAndTypes.contains("{"+ctx.expr().get(i).location().ID().getText()+","+"boolean"+"}") || !this.variaveisAndTypes.contains("{"+ctx.expr().get(i).location().ID().getText()+","+"boolean"+","+"array"+"}")){
                        this.error(ctx.expr().get(i).location().ID().getSymbol(), "condition should not be a boolean");
                        System.exit(0);
                    }
                }
            }

            if(ctx.FOR() != null){
                if(ctx.expr().get(0).getText().equals("true") || ctx.expr().get(0).getText().equals("false") || !this.variaveisAndTypes.contains("{"+ctx.expr().get(0).location().ID().getText()+","+"int"+"}")){
                    this.error(ctx.ID().getSymbol(),"initial condition must be an int");
                    System.exit(0);
                }
            }
            //String var = ctx.location().ID().getText();
            if (!this.variaveis.contains(ctx.location().ID().getText())) {
                this.error(ctx.location().ID().getSymbol(), "identifier used before being declared");
                System.exit(0);
            }

            if(ctx.location().COLCHETERIGTH() != null){
                try{
                    int verify = Integer.parseInt(ctx.location().expr().getText());
                }catch (NumberFormatException e){
                    if(!this.variaveisAndTypes.contains("{"+ctx.location().expr().getText()+","+"int"+"}")){
                        this.error(ctx.location().ID().getSymbol(),"array index has wrong type");
                        System.exit(0);
                    }
                }
            }

            if(ctx.assing_op().getText() != null){
                String verifyType = "";
                String verifyArray = "";

                if(this.variaveisAndTypes.contains("{"+ctx.location().ID().getText()+","+"int"+","+"array"+"}")){
                    verifyType = "int";
                    verifyArray = "array";
                }else if(this.variaveisAndTypes.contains("{"+ctx.location().ID().getText()+","+"boolean"+","+"array"+"}")){
                    verifyType = "boolean";
                    verifyArray = "array";
                }else if (this.variaveisAndTypes.contains("{"+ctx.location().ID().getText()+","+"int"+"}")){
                    verifyType = "int";
                    verifyArray = null;
                }else if (this.variaveisAndTypes.contains("{"+ctx.location().ID().getText()+","+"boolean"+"}")){
                    verifyType = "boolean";
                    verifyArray = null;
                }

                if(ctx.assing_op().SINAL_MAIS_IGUAL() != null || ctx.assing_op().SINAL_MENOS_IGUAL() != null){
                    if(!verifyType.equals("int") || ctx.expr().get(0).literal().int_literal() == null){
                        this.error(ctx.location().ID().getSymbol(), "lhs and rhs of += must be int");
                        System.exit(0);
                    }
                }

                for(int i=0;i<ctx.expr().size();i++){
                    if(verifyType == "int" && this.variaveisAndTypes.contains("{"+ctx.expr().get(i).getText()+","+"int"+","+"array"+"}")){
                        this.error(ctx.location().ID().getSymbol(), "bad type, rhs should be an int");
                        System.exit(0);
                    }
                    if (verifyType=="int" && ctx.expr().get(i).bin_op().rel_op().getText() != null){
                            this.error(ctx.location().ID().getSymbol(),"rhs should be an int expression");
                            System.exit(0);
                        }

                    if(ctx.expr().get(i).getText().contains("!")){
                        if(!ctx.expr().get(i).expr().get(0).getText().equals("true")&& !ctx.expr().get(i).expr().get(0).getText().equals("false")){
                            this.error(ctx.location().ID().getSymbol(),"operand of ! must be boolean");
                            System.exit(0);
                        }
                    }
                }

                if(ctx.expr().get(0).bin_op().rel_op() != null){
                    for(int i=0;i<ctx.expr().get(0).expr().size();i++){
                        if(ctx.expr().get(0).expr(i).getText().equals("true") || ctx.expr().get(0).expr(i).getText().equals("false")){
                            this.error(ctx.location().ID().getSymbol(),"operands of > must be ints");
                            System.exit(0);
                        }
                    }
                }

                if (ctx.expr().get(0).bin_op().eq_op() != null) {
                    for (int i = 0; i < ctx.expr().get(0).expr().size(); i++) {
                        if (ctx.expr().get(0).expr(i).getText().equals("true") || ctx.expr().get(0).expr(i).getText().equals("false")){
                            this.error(ctx.location().ID().getSymbol(),"types of operands of == must be equal");
                            System.exit(0);
                        }
                    }
                }
            }
        } catch (Exception e) {}
    }

    @Override
    public void exitStatem(DecafParser.StatemContext ctx) {
    }

    @Override
    public void enterDecl(DecafParser.DeclContext ctx) {
        defineVar(ctx.type(), ctx.ID().getSymbol());
        this.variaveis.add(ctx.ID().getText());
        this.variaveisAndTypes.add("{"+ctx.ID().getText()+","+ctx.type().getText()+"}");
    }

    @Override
    public void exitDecl(DecafParser.DeclContext ctx) {
        String name = ctx.ID().getSymbol().getText();
        Symbol var = currentScope.resolve(name);
        if (var == null) {
            this.error(ctx.ID().getSymbol(), "no such variable: " + name);
        }
        if (var instanceof FunctionSymbol) {
            this.error(ctx.ID().getSymbol(), name + " is not a variable");
        }
    }

    @Override
    public void enterVar_decl(DecafParser.Var_declContext ctx) {
        for (int i = 0; i < ctx.ID().size(); i++) {
            this.variaveis.add(ctx.ID().get(i).getText());
            this.variaveisAndTypes.add("{"+ctx.ID().get(i).getText()+","+ctx.decl().type().getText()+"}");
            defineVar(ctx.ID().get(i).getSymbol());
        }
    }

    @Override
    public void exitVar_decl(DecafParser.Var_declContext ctx) {
        for (int i = 0; i < ctx.ID().size(); i++) {
            String name = ctx.ID().get(i).getSymbol().getText();
            Symbol var = currentScope.resolve(name);

            if (var == null) {
                this.error(ctx.ID().get(i).getSymbol(), "not such variable " + name);
            }
            if (var instanceof FunctionSymbol) {
                this.error(ctx.ID().get(i).getSymbol(), name + " is not a variable");
            }
        }
    }

    @Override
    public void enterType_id(DecafParser.Type_idContext ctx) {
        defineVar(ctx.type(), ctx.ID().getSymbol());
        this.variaveis.add(ctx.ID().getText());
    }

    @Override
    public void exitType_id(DecafParser.Type_idContext ctx) {
        String name = ctx.ID().getSymbol().getText();
        Symbol var = currentScope.resolve(name);
        if (var == null) {
            this.error(ctx.ID().getSymbol(), "no such variable: " + name);
        }
        if (var instanceof FunctionSymbol) {
            this.error(ctx.ID().getSymbol(), name + " is not a variable");
        }
    }

    void defineVar(DecafParser.TypeContext typeCtx, Token nameToken) {
        int typeTokenType = typeCtx.start.getType();
        VariableSymbol var = new VariableSymbol(nameToken.getText());

        // DecafSymbol.Type type = this.getType(typeTokenType);
        // var.setType(type);

        currentScope.define(var); // Define symbol in current scope
    }

    void defineVar(Token nameToken) {
        VariableSymbol var = new VariableSymbol(nameToken.getText());

        // DecafSymbol.Type type = this.getType(typeTokenType);
        // var.setType(type);

        currentScope.define(var); // Define symbol in current scope
    }

    /**
     * Método que atuliza o escopo para o atual e imprime o valor
     *
     * @param s
     */
    private void pushScope(Scope s) {
        currentScope = s;
        System.out.println("entering: " + currentScope.getName() + ":" + s);
    }

    /**
     * Método que cria um novo escopo no contexto fornecido
     *
     * @param ctx
     * @param s
     */
    void saveScope(ParserRuleContext ctx, Scope s) {
        scopes.put(ctx, s);
    }

    /**
     * Muda para o contexto superior e atualia o escopo
     */
    private void popScope() {
        System.out.println("leaving: " + currentScope.getName() + ":" + currentScope);
        currentScope = currentScope.getEnclosingScope();
    }

    public static void error(Token t, String msg) {
        System.err.printf("line %d:%d %s\n", t.getLine(), t.getCharPositionInLine(), msg);
    }

    /**
     * Valida tipos encontrados na linguagem para tipos reais
     *
     * @param tokenType
     * @return
     */
    public static DecafSymbol.Type getType(int tokenType) {
        switch (tokenType) {
        case DecafParser.VOID:
            return DecafSymbol.Type.tVOID;
        case DecafParser.INTLIT:
            return DecafSymbol.Type.tINT;
        }
        return DecafSymbol.Type.tINVALID;
    }

    /**
     *  Verificar se um texto só tem numeros
     * 
     * @param texto
     * @return
     */

     public boolean SoTemNumero(String texto) {
         for(int i =0; i<texto.length();i++){
             if(!Character.isDigit(texto.charAt(i))){
                 return false;
             }
         }
         return true;
     }

}
