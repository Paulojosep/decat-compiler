parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}

program: CLASS ID KEYRIGTH (field_decl)* (method_decl)* KEYLEFT EOF;

declar : decl (VIRGULA ID)* PONTOVIRGULA;

type : INT | BOOLEAN;

digit : NUM;

bool : TRUE | FALSE;

bin_op : arith_op 
	| rel_op 
	| eq_op 
	| cond_op;

arith_op : SUM 
	| SUB 
	| MULTI 
	| DIV 
	| PORCENTAGEM ;

rel_op : MAIOR 
	| MENOR 
	| MAIORIGUAL 
	| MENORIGUAL;

eq_op : EQUAL 
	| DIFER;

cond_op : AND 
	| OR;

assing_op : RECEBE | SINAL_MAIS_IGUAL | SINAL_MENOS_IGUAL;

decl : type ID;

field_decl : (type ID | type ID COLCHETERIGTH? int_literal COLCHETELEFT) (VIRGULA ID)* PONTOVIRGULA;

method_decl : (type | VOID) ID PARENTRIGHT ((decl)(VIRGULA decl)*)? PARENTLEFT block;

block : KEYRIGTH (var_decl)* (statem)* KEYLEFT;

var_decl : decl (VIRGULA ID)* PONTOVIRGULA;

statem : location assing_op expr PONTOVIRGULA 
	| method_call PONTOVIRGULA 
	| IF PARENTRIGHT expr PARENTLEFT block (ELSE block)? 
	| FOR ID RECEBE expr VIRGULA expr block 
	| RETURN (expr)? PONTOVIRGULA  
	| BREAK PONTOVIRGULA  
	| CONTINUE PONTOVIRGULA  
	| block;

method_call : method_name PARENTRIGHT ((expr) ((VIRGULA expr)+)?)? PARENTLEFT 
	     | CALLOUT PARENTRIGHT STRING (VIRGULA callout_arg(VIRGULA callout_arg)*)? PARENTLEFT;

callout_arg : expr 
	     | string_literal;

expr : location 
       | method_call 
       | literal
       | expr bin_op expr 
       | SUB expr 
       | EXCLAMACAO expr 
       | PARENTRIGHT expr PARENTLEFT;

method_name : ID;

location : ID 
	   | ID COLCHETERIGTH expr COLCHETELEFT;

literal : int_literal 
	| char_literal 
	| bool_literal;

int_literal : INTLIT 
	| HEXLIT;

char_literal : CHAR;

bool_literal : bool;

string_literal : STRING;

type_id : type ID;

